// app/routes.js

    module.exports = function(app) {
        let moment = require('moment');

        // server routes ===========================================================
        // handle things like api calls
        // authentication routes

        // sample api route
        app.get('/api/customer_visits/:start/:end', function(req, res) {
            let start = req.params.start;
            let end = req.params.end;
            let visits = [];
            for(i=0; i < req.db.customer_visits.length; i++){
                console.log(req.db.customer_visits[i]);
              let event = moment.utc(req.db.customer_visits[i].visit_start).valueOf();
              if(event > start && event < end) {
                visits.push(req.db.customer_visits[i]);
              }
            }

            if (visits.length > 0){
                res.json(visits);
            }
            else {
                res.status(404).send('Not found');
            }
        });

        app.get('/api/visit_events/:id', function(req, res) {
            let events = [];
            for(i=0; i < req.db.visit_events.length; i++){
                if(req.db.visit_events[i].visit_id == req.params.id){
                    events.push(req.db.visit_events[i])
                }
            }

            if (events.length > 0){
                res.json(events);
            }
            else {
                res.status(404).send('Not found');
            }
        });

        // route to handle creating goes here (app.post)
        // route to handle delete goes here (app.delete)

        // frontend routes =========================================================
        // route to handle all angular requests
        app.get('*', function(req, res) {
            res.sendfile('./public/views/index.html'); // load our public/index.html file
        });

    };

