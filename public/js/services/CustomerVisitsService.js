// public/js/services/CustomerVisitsService.js
angular.module('CustomerVisitsService', []).factory('CustomerVisits', ['$q', '$http', '$rootScope', function($q, $http, $rootScope) {

    return {
        getVisits : function() {
        	return $q((resolve, reject) => {
        		let now = moment().startOf('day').utc();
	    			let later = moment().startOf('day').add(23,'hours').utc();
	          $http.get('/api/customer_visits/' + now + '/' + later).then(function(visits) {
			    		resolve(visits.data.length > 0 ? visits.data : null);
			    	}, function(error) {
			        reject(console.log('CustomerVisitsService' + JSON.stringify(error)));
			    	});
        	});
        },

        findCurrentVisit: (visits) => {
          for(i=0; i < visits.length; i++){
            let start = moment.utc(visits[i].visit_start).valueOf();
            let now = moment.utc().valueOf();
            let end = moment.utc(visits[i].visit_end).valueOf();
            if(now > start && now < end) {
              return visits[i];
            }
          }
          return null;
        }  
    } 

}]);
