// public/js/services/VisitEventsService.js
angular.module('VisitEventsService', []).factory('VisitEvents', ['$q', '$http', '$rootScope', function($q, $http, $rootScope) {  

    return {
        get : function(id) {
        	return $q((resolve, reject) => {
        		  $http.get('/api/visit_events/' + id).then(function(events){
    						resolve(events.data.length > 0 ? events : null);
        			}, function(error) {
		        		reject(console.log('VisitEventsService ' + error));
		 					});
        	});
        },

        findCurrentEvents: (events) => {
          let now = moment().utc().valueOf();
          let eventArray = [];
          for(i=0; i < events.length; i++){
            let end = moment.utc(events[i].event_end).valueOf();
            if(now < end) {
              eventArray.push(events[i]);
            }
          }
          return eventArray;
        },

        sortCurrentEvents: (events) => {
          return events.sort((a, b) => {
            return moment.utc(a.event_start).valueOf() - moment.utc(b.event_start).valueOf();
          }); 
        }
    }    

}]);
