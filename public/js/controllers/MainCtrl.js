// public/js/controllers/MainCtrl.js
angular.module('MainCtrl', []).controller('MainController', function($scope, $interval, CustomerVisits, VisitEvents) {
    let cycleTimer = null;

    let getInfo = () => {
      CustomerVisits.getVisits().then(function(visits) {
        $scope.currentVisitor = CustomerVisits.findCurrentVisit(visits);
        modifyBackground($scope.currentVisitor);
        if($scope.currentVisitor) {
      		VisitEvents.get($scope.currentVisitor.visit_id).then(function(events){
            $scope.currentEvents = VisitEvents.sortCurrentEvents(VisitEvents.findCurrentEvents(events.data));
            cycleScreens();
          }, function(error){
        		console.log(error);
          });
        }
        else {
          $scope.currentEvents = null;
          $interval.cancel(cycleTimer);
          $scope.agenda = false;
          cycleTimer = null;
        }
      }, function(error) {
          console.log(error);
      });
    }

    let timeCheck = () => {//checks to see if a visitor is here now
        $interval(() => {
          getInfo();
        }, 3000);
    };

    let modifyBackground = (visit) => {
      $scope.welcome = true;
      let industry = visit ? visit.customer_industry : '';
      switch(industry) {
          case 'healthcare':
              $scope.image = './backgrounds/healthcare.jpg';
              break;
          case 'education':
              $scope.image = './backgrounds/education.jpg';
              break;
          case 'banking':
              $scope.image = './backgrounds/banking.jpg';
              break;
          case 'tech':
              $scope.image = './backgrounds/tech.jpg';
              break;
          default:
              $scope.welcome = false;
              $scope.image = './backgrounds/general.jpg';
      }
    };

    let cycleScreens = () => {
      let allowCycle = $scope.currentEvents != null && $scope.currentEvents.length > 0 ? true : false;
      if($scope.currentVisitor && cycleTimer == null && allowCycle){
        cycleTimer = $interval(() => {
            $scope.agenda = !$scope.agenda;
        }, 2000);
      }
      else if(!$scope.currentVisitor || !allowCycle){
        $interval.cancel(cycleTimer);
        $scope.agenda = false;
        cycleTimer = null;
      }
    };

    timeCheck();

});